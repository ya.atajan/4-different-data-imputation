import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class a2_435 {
    private static String nameToUse, maePrefix, maePostfix;
    private static String[][] data;
    private static String[][] completeData;
    private static double[] meanImputedFeatures = new double[13];
    private static double[] condImputedFeaturesY = new double[13];
    private static double[] condImputedFeaturesN = new double[13];
    private static List<Integer> actualImputedRows = new ArrayList<>();
    private static List<Integer> imputedColumns = new ArrayList<>();
    private static List<Double> imputedValues = new ArrayList<>();
    private static int rowCount;

    // Start of a rounding function
    private static double roundTo(double value, int place) {
        if (place < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(place, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    // Start of MAE calculation function
    private static String maeCalculator() {
        int n = imputedValues.size();
        double total = 0;
        double mae;

        for (int i = 0; i < n; i ++) {
            total += Math.abs(imputedValues.get(i) - Double.parseDouble(completeData[actualImputedRows.get(i)][imputedColumns.get(i)]));
        }

        mae = (total/n);

        System.out.println(" ");
        return maePrefix + maePostfix + " = " + roundTo(mae, 4);
    }

    // Start of conditional hot deck imputation algorithm
    private static void condHotDeckImp() throws FileNotFoundException {
        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < data[r].length; c++) {
                if (data[r][c].equals("?") && data[r][13].equals("Y")) {
                    data[r][c] = String.valueOf(euclideanDist(r, 'Y')); // Send each row with missing value
                    System.out.print("\rCurrently at Y row: " + r);
                    actualImputedRows.add(r);
                    imputedColumns.add(c);
                } else if (data[r][c].equals("?") && data[r][13].equals("N")) {
                    data[r][c] = String.valueOf(euclideanDist(r, 'N')); // Send each row with missing value
                    System.out.print("\rCurrently at N row: " + r);
                    actualImputedRows.add(r);
                    imputedColumns.add(c);
                }
            }
        }
        writeCSV("imputed_hd_conditional");
    }

    // Start of hot deck imputation algorithm
    private static void hotDeckImp() throws FileNotFoundException {
        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < data[r].length; c++) {
                if (data[r][c].equals("?")) {
                    data[r][c] = String.valueOf(euclideanDist(r, 'A')); // Send each row with missing value
                    System.out.print("\rCurrently at row: " + r);
                    actualImputedRows.add(r);
                    imputedColumns.add(c);
                }
            }
        }
        writeCSV("imputed_hd");
    }

    // Start of euclidean distance algorithm
    private static double euclideanDist(int missing, Character conditional) {
        double[] missingRow = new double[13];
        double[] candidates = new double[rowCount];
        double xDiff = 0.0;
        int missingIndex = 0;
        double[] minArray = {999.99, 999.99, 999.99, 999.99, 999.99};
        int[] indexArray = {0, 0, 0, 0, 0};
        double imputed = 999.01;
        //imputedRows.add(missing);

        // Column that is missing a value
        for (int c = 0; c < data[missing].length - 1; c++) { // Skip non-numeric class feature
            if (!data[missing][c].equals("?")) {
                missingRow[c] = Double.parseDouble(data[missing][c]);
            } else {
                missingRow[c] = 0.0;
                missingIndex = c;
            }
        }

        for (int r = 0; r < rowCount; r++) {
            if (r + 1 != missing && !actualImputedRows.contains(r)) { // Skip the row to be imputed
                if (conditional == 'A') {
                    for (int c = 0; c < data[r].length - 1; c++) { // Skip non-numeric class feature
                        if (!data[r][c].equals("?")) {
                            xDiff += Math.pow((missingRow[c] - Double.parseDouble(data[r][c])), 2);
                        } else {
                            xDiff += 1;
                        }
                    }
                } else {
                    for (int c = 0; c < data[r].length - 1; c++) { // Skip non-numeric class feature
                        if (!data[r][c].equals("?") && data[r][13].equals(String.valueOf(conditional))) {
                            xDiff += Math.pow((missingRow[c] - Double.parseDouble(data[r][c])), 2);
                        } else {
                            xDiff += 1;
                        }
                    }
                }
                candidates[r] = Math.sqrt(xDiff);
                xDiff = 0.0; // Reset xDiff
            } else {
                candidates[r] = 999.99;
            }
        }

        for (int i = 1; i < candidates.length; i++) {
            for (int j = 0; j < minArray.length; j++) {
                if (candidates[i] < minArray[j]) {
                    minArray[j] = candidates[i];
                    indexArray[j] = i;
                    break;
                }
            }
        }

        for (int i = 0; i < 5; i++) {
            if (!data[indexArray[i]][missingIndex].equals("?")) {
                imputed = Double.parseDouble(data[indexArray[i]][missingIndex]);
                break;
            }
        }
        imputedValues.add(imputed);
        return roundTo(imputed, 5);
    }

    // Start of mean imputation algorithm
    private static void condMeanImp() throws FileNotFoundException {
        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < data[r].length; c++) {
                if (data[r][c].equals("?") && data[r][13].equals("Y")) {
                    if (condImputedFeaturesY[c] != 0) {
                        data[r][c] = String.valueOf(condImputedFeaturesY[c]);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(condImputedFeaturesY[c]);
                    } else {
                        double imputation = mean(c, 'Y');
                        condImputedFeaturesY[c] = imputation;
                        data[r][c] = String.valueOf(imputation);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(imputation);
                    }
                } else if (data[r][c].equals("?") && data[r][13].equals("N")) {
                    if (condImputedFeaturesN[c] != 0) {
                        data[r][c] = String.valueOf(condImputedFeaturesN[c]);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(condImputedFeaturesN[c]);
                    } else {
                        double imputation = mean(c, 'N');
                        condImputedFeaturesN[c] = imputation;
                        data[r][c] = String.valueOf(imputation);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(imputation);
                    }
                }
            }
        }
        writeCSV("imputed_mean_conditional");
    }

    // Start of mean imputation algorithm
    private static void meanImp() throws FileNotFoundException {
        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < data[r].length; c++) {
                if (data[r][c].equals("?")) {
                    if (meanImputedFeatures[c] != 0) {
                        data[r][c] = String.valueOf(meanImputedFeatures[c]);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(meanImputedFeatures[c]);
                    } else {
                        double imputation = mean(c, 'A');
                        meanImputedFeatures[c] = imputation;
                        data[r][c] = String.valueOf(imputation);
                        actualImputedRows.add(r);
                        imputedColumns.add(c);
                        imputedValues.add(imputation);
                    }
                }
            }
        }
        writeCSV("imputed_mean");
//        System.out.println("Mean imputation for each feature are: " + Arrays.toString(meanImputedFeatures));
    }

    // Get's mean value from the entire column excluding the missing values
    private static double mean(Integer column, Character conditional) {
        int i = 0;
        int denom = 0;
        double total = 0.0;

        if (conditional == 'A') {
            while (i < rowCount) {
                if (!data[i][column].equals("?")) {
                    total = Double.parseDouble((data[i][column])) + total;
                    denom += 1;
                    i += 1;
                } else {
                    i += 1;
                }
            }
        } else {
            while (i < rowCount) {
                if (!data[i][column].equals("?") && data[i][13].equals(String.valueOf(conditional))) {
                    total = Double.parseDouble((data[i][column])) + total;
                    denom += 1;
                    i += 1;
                } else {
                    i += 1;
                }
            }
        }
        return roundTo((total / denom), 5);
    }

    // Writes to csv file named accordingly to the algorithm
    private static void writeCSV(String name) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File(nameToUse + name + ".csv"));

        for (int r = 0; r < rowCount; r++) {
            for (int c = 0; c < data[r].length; c++) {
                if ((c + 1) % 14 == 0) {
                    pw.write(data[r][c]);
                    pw.write('\n');
                } else {
                    pw.write(data[r][c]);
                    pw.write(',');
                }
            }
        }
        pw.close();
    }

    // Converts csv file to string array list
    private static void readCSV() throws FileNotFoundException {
        List<String[]> features = new ArrayList<>();
        List<String[]> featuresComplete = new ArrayList<>();
        Scanner userInput = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter the path to the complete data-set CSV file: ");
        String completeDataPath = userInput.next();
        System.out.println("Enter the path to the incomplete data-set CSV file: ");
        String absPath = userInput.next();
        if (absPath.contains("_missing01")) {
            nameToUse = "V00742051_missing01_";
            maePrefix = "MAE_01_";
        } else if (absPath.contains("_missing10")) {
            nameToUse = "V00742051_missing10_";
            maePrefix = "MAE_10_";
        } else {
            nameToUse = "V00742051_missingXX_";
        }
        System.out.println("Choose the calculation you want to perform: (NOTE: 3 and 4 will take few seconds to process)\n1: Mean Imputation\n2: Conditional Mean imputation\n3: Hot deck imputation\n4: Conditional hot deck imputation");
        int calculation = userInput.nextInt();
        Scanner reader = new Scanner(new File(absPath));
        reader.nextLine(); // Skip csv header

        while ((reader.hasNext())) {
            features.add(reader.nextLine().split(","));
        }

        reader = new Scanner(new File(completeDataPath));
        reader.nextLine(); // Skip csv header

        while ((reader.hasNext())) {
            featuresComplete.add(reader.nextLine().split(","));
        }

        reader.close();
        userInput.close();
        to2dArray(features, featuresComplete, calculation);
    }

    // Converts string array list to 2D array
    private static void to2dArray(List<String[]> list, List<String[]> compList, int calculation) throws FileNotFoundException {
        // convert our list to a String array.
        data = new String[list.size()][0];
        list.toArray(data);
        rowCount = data.length;

        completeData = new String[compList.size()][0];
        compList.toArray(completeData);

        switch (calculation) {
            case 1:
                maePostfix = "mean";
                meanImp();
                System.out.println(maeCalculator());
                break;
            case 2:
                maePostfix = "mean_conditional";
                condMeanImp();
                System.out.println(maeCalculator());
                break;
            case 3:
                maePostfix = "hd";
                hotDeckImp();
                System.out.println(maeCalculator());
                break;
            case 4:
                maePostfix = "hd_conditional";
                condHotDeckImp();
                System.out.println(maeCalculator());
                break;
        }
    }

    public static void main(String[] args) {
        try {
            readCSV();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}