# 4 different data imputation

Implement and evaluate four algorithms for the imputation of missing values using two provided datasets. 

1. Mean imputation
2. Conditional mean imputation
3. Hot-deck imputation
4. Conditional hot-deck imputation